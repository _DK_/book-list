Application can be deployed by running
  `php artisan serve`

To run locally, 'DB_DATABASE' in '.env' must be changed to reflect the absolute path of 
  `database/database.sqlite`