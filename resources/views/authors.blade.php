@extends('layout')

@section('content')
	<table class="table">
		<thead>
			<tr>
				<th scope="col">Author Name</th>
				<th scope="col">Update</th>
			</tr>
		</thead>
		<tbody>
			@foreach($authors as $author)
				<tr>
					<form method="POST" action="/authors/update/{{ $author->id }}">
						@method('PUT')
						@csrf
						<td>
							<input type="text" class="form-control" id="authorName" value="{{ $author->name}}" name="authorName" required>
						</td>
						<td><button type="submit" class="btn btn-primary">Update</button></td>
					</form>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection()