<!DOCTYPE html>
<html>
<head>
    <title>Books</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<a class="navbar-brand" href="#">Books!</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="/">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/books">Books</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/authors">Authors</a>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0" method="POST" action="/books/search">
					@csrf
					<input class="form-control mr-sm-2" type="search" placeholder="Search by book or author" aria-label="Search" name="search" required>
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>
			</div>
		</div>
	</nav>

	<br />

    <div class="container">
    	<div class="row>">
    		@yield('content')
    	</div>
		<br />
    	<div class="row">
			<form class="form my-2 my-lg-0" method="GET" action="/books/export">
				<div class="form-group">
					<label for="exportData">Select data to export</label>
					<select class="form-control" id="exportData" name="exportData">
						<option value="both">Export Titles and Authors</option>
						<option value="titles">Export Titles</option>
						<option value="authors">Export Authors</option>
					</select>
				</div>
				<div class="form-group">
					<label for="exportData">Select export format</label>
					<select class="form-control" id="exportData" name="exportFormat">
						<option value="csv">CSV</option>
						<option value="xml">XML</option>
					</select>
				</div>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Export</button>
			</form>
		</div>
	</div>

</body>
</html>