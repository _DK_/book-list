@extends('layout')

@section('content')
	<form method="POST" action="/books/create">
		@csrf
		<div class="form-group row">
			<label class="col-sm-2" for="bookTitle">Title <span class="text-danger">*</span></label>
    		<div class="col-sm-10">
				<input type="text" class="form-control" id="bookTitle" placeholder="Title" name="bookTitle" required>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2" for="authorName">Author <span class="text-danger">*</span></label>
    		<div class="col-sm-10">
				<input type="text" class="form-control" id="authorName" placeholder="Author" name="authorName" required>
			</div>
		</div>
		<hr />
		<button type="submit" class="btn btn-primary">Add</button>
	</form>
	<br />
	<table class="table">
		<thead>
			<tr>
				<th scope="col">
					Title
					<a href="/books?sortBy=book&sort=asc"><i class="fas fa-arrow-up"></i></a>
					<a href="/books?sortBy=book&sort=desc"><i class="fas fa-arrow-down"></i></a>
				</th>
				<th scope="col">
					Author
					<a href="/books?sortBy=author&sort=asc"><i class="fas fa-arrow-up"></i></a>
					<a href="/books?sortBy=author&sort=desc"><i class="fas fa-arrow-down"></i></a>
				</th>
				<th scope="col">Delete</th>
			</tr>
		</thead>
		<tbody>
			@if ($books != null)
				@foreach($books as $book)
					<tr>
						<td>{{ $book->title }}</td>
						<td>{{ $book->author->name }}</td>
						<td><a href="/books/delete/{{ $book->id }}"><i class="fas fa-trash-alt"></i></a></td>
					</tr>
				@endforeach
			@else
				<tr>
					<td>No results</td>
				</tr>
			@endif
		</tbody>
	</table>
@endsection()