<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'BooksController@index');
Route::get('/books', 'BooksController@index');
Route::post('/books/create', 'BooksController@create');
Route::get('/books/delete/{book}', 'BooksController@delete');
Route::post('/books/search', 'BooksController@search');

Route::get('/authors', 'AuthorsController@index');
Route::put('/authors/update/{id}', 'AuthorsController@update');

Route::get('/books/export', 'BooksController@export');