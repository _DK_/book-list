<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Book;
use App\Author;

class BooksController extends Controller
{
    public function index(Request $request) {
    	$books = Book::all();

    	$sortBy = $request->sortBy;
		$sort = $request->sort;

		if ($sortBy != null && $sort != null) {
			if ($sortBy == "author" && $sort == 'asc') {
				$books = $books->sortBy('author.name');
			}
			if ($sortBy == "author" && $sort == 'desc') {
				$books = $books->sortByDesc('author.name');
			}
			if ($sortBy == "book" && $sort == 'asc') {
				$books = $books->sortBy('title');
			}
			if ($sortBy == "book" && $sort == 'desc') {
				$books = $books->sortByDesc('title');
			}
		}

        return view('books', ['books' => $books]);
    }

    public function create(Request $request) {
    	$bookTitle = $request->bookTitle;
    	$authorName = $request->authorName;

    	$author = Author::where('name', '=', $authorName)->first();

		if ($author == null) {
			$author = new Author;
			$author->name = $authorName;
			$author->save();
		}

		$book = new Book;
		$book->title = $bookTitle;
		$book->author_id = $author->id;
		$book->save();

    	return redirect('/books');
    }

    public function delete(Book $book) {
    	$author = $book->author;
    	$book->delete();

    	if ($author->books()->first() == null) {
    		$author->delete();
    	}
    	
    	return redirect('/books');
    }

    public function search(Request $request) {
    	$term = $request->search;

    	$byBookTitle = Book::where('title', '=', $term)->get();
    	$author = Author::where('name', '=', $term)->first();
    	if ($author != null) {
	    	$byAuthor = Book::where('author_id', '=', $author->id)->get();
	    	$combined = $byBookTitle->merge($byAuthor);
        	return view('books', ['books' => $combined->all()]);
    	}

        return view('books', ['books' => $byBookTitle->all()]);
    }

    public function export(Request $request) {
    	$data = $request->exportData;
    	$format = $request->exportFormat;

    	$books = Book::all();
    	$authors = Author::all();

    	if ($format == 'csv') {
    		$export = '';
	        foreach($books as $book) {
	        	if ($data == 'both') {
	            	$export .= $book->title . ', ' . $book->author->name . ' <br/>';
	            } else if ($data == 'titles') {
	            	$export .= $book->title . ' <br/>';
	            } else if ($data == 'authors') {
	            	$export .= $book->author->name . ' <br/>';
	            }
	        }
	        return $export;
    	}

    	if ($format == 'xml') {
    		$export = '&lt;books&gt;';
	        foreach($books as $book) {
	        	if ($data == 'both') {
	            	$export .= '&lt;book&gt;&lt;title&gt;' . $book->title . '&lt;/title&gt;&lt;author&gt;' . $book->author->name . '&lt;/author&gt;&lt;/book&gt;';
	            } else if ($data == 'titles') {
	            	$export .= '&lt;book&gt;&lt;title&gt;' . $book->title . '&lt;/title&gt;&lt;/book&gt;';
	            } else if ($data == 'authors') {
	            	$export .= '&lt;book&gt;&lt;author&gt;' . $book->author->name . '&lt;/author&gt;&lt;/book&gt;';
	            }
	        }
	        $export .= '&lt;/books&gt;';
	        return $export;
    	}

    	return redirect('/');
    }
}
