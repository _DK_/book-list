<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Author;

class AuthorsController extends Controller
{
    public function index() {
    	$authors = Author::all();
        return view('authors', ['authors' => $authors]);
    }

    public function update($id, Request $request) {
    	$author = Author::where('id', '=', $id)->first();
    	$author->name = ($request->authorName != '') ? $request->authorName : null;
    	$author->save();
    	return redirect('/authors');
    }
}
