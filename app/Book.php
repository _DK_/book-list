<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * Get the phone record associated with the user.
     */
    public function author()
    {
        return $this->belongsTo('App\Author');
    }
}
