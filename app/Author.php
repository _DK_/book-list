<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    /**
     * Define the association between books and authors
     */
    public function books()
    {
        return $this->hasMany('App\Book');
    }
}
