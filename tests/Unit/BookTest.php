<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\QueryException;
use App\Author;
use App\Book;

class BookTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Testing the basic functionality of Book
     *
     * @return void
     */
    public function testAuthor()
    {
    	$author = new Author;
    	$author->name = "Test Book";
        $author->save();

        $book = new Book;
        $book->title = "Test Title";
        $book->author_id = $author->id;
        $this->assertTrue($book->save());

        $storedBook = Book::all()->first();
        $this->assertTrue($storedBook->title == $book->title);
    }

    /**
     * Testing the title field of Book
     *
     * @return void
     */
    public function testBookTitleNotNull()
    {
        $this->expectException(QueryException::class);
    	$author = new Author;
    	$author->name = "Test Author";
        $author->save();

        $book = new Book;
        $book->title = null;
        $book->author_id = $author->id;
        $book->save();
    }

    /**
     * Testing the author_id field of Book
     *
     * @return void
     */
    public function testBookAuthorIdNotNull()
    {
        $this->expectException(QueryException::class);

        $book = new Book;
        $book->title = "Test Title";
        $book->author_id = null;
        $book->save();
    }
}
