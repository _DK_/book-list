<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\QueryException;
use App\Author;

class AuthorTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Testing the basic functionality of Author
     *
     * @return void
     */
    public function testAuthor()
    {
    	$author = new Author;

    	$author->name = "Test Author";
        $this->assertTrue($author->save());

        $storedAuthor = Author::all()->first();
        $this->assertTrue($storedAuthor->name == $author->name);
    }

    /**
     * Testing the name field of Author
     *
     * @return void
     */
    public function testAuthorNameNotNull()
    {
        $this->expectException(QueryException::class);
    	$author = new Author;

    	$author->name = null;
    	$author->save();
    }
}
